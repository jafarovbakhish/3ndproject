public class StandardRoom extends Room {
    public StandardRoom(int roomNumber, double nightlyRate) {
        super(roomNumber, nightlyRate);
    }

    @Override
    public double calculateCharges(int numberOfNights) {
        return getNightlyRate() * numberOfNights;
    }
}


