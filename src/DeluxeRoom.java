public class DeluxeRoom extends Room {
    private int numberOfBeds;

    public DeluxeRoom(int roomNumber, double nightlyRate, int numberOfBeds) {
        super(roomNumber, nightlyRate);
        this.numberOfBeds = numberOfBeds;
    }

    public int getNumberOfBeds() {
        return numberOfBeds;
    }

    @Override
    public double calculateCharges(int numberOfNights) {
        return getNightlyRate() * numberOfNights;
    }
}

