    public class Main {
    public static void main(String[] args) {

                Room room1 = new StandardRoom(1001, 70.0);
                Room room2 = new DeluxeRoom(2001, 150.0, 3);
                Room room3 = new Suite(3001, 5000.0, 4,true);


                room2.book();
                room3.book();


                room1.checkAvailability();
                room2.checkAvailability();
                room3.checkAvailability();


                int numberOfNights = 3;
                double chargesRoom1 = room1.calculateCharges(numberOfNights);
                double chargesRoom2 = room2.calculateCharges(numberOfNights);
                double chargesRoom3 = room3.calculateCharges(numberOfNights);

                System.out.println("Room 1: " + chargesRoom1+"$");
                System.out.println("Room 2: " + chargesRoom2+"$");
                System.out.println("Room 3: " + chargesRoom3+"$");
            }
        }

