public class Suite extends Room {
    private int numberOfBeds;
    private boolean hasLivingRoom;

    public Suite(int roomNumber, double nightlyRate, int numberOfBeds, boolean hasLivingRoom) {
        super(roomNumber, nightlyRate);
        this.numberOfBeds = numberOfBeds;
        this.hasLivingRoom = hasLivingRoom;
    }

    public int getNumberOfBeds() {
        return numberOfBeds;
    }

    public boolean hasLivingRoom() {
        return hasLivingRoom;
    }

    @Override
    public double calculateCharges(int numberOfNights) {
        return getNightlyRate() * numberOfNights;
    }
}

